package com.epam.service;

import com.epam.DAO.implementation.ProductDAOImpl;
import com.epam.model.ProductModel;

import java.sql.SQLException;
import java.util.List;

public class ProductService {
    public List<ProductModel> findAll() throws SQLException {
        return new ProductDAOImpl().findAll();
    }

    public ProductModel findById(String s) throws SQLException{
        return new ProductDAOImpl().findById(s);
    }

    public int create(ProductModel entity) throws SQLException{
        return new ProductDAOImpl().create(entity);
    }

    public int update(ProductModel entity) throws SQLException{
        return new ProductDAOImpl().update(entity);
    }

    public int delete(String s) throws SQLException{
        return new ProductDAOImpl().delete(s);
    }
}
