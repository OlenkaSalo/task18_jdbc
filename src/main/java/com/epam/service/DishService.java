package com.epam.service;

import com.epam.DAO.implementation.DishDAOImpl;
import com.epam.DAO.implementation.DishStoreDAOImpl;
import com.epam.model.DishModel;
import com.epam.model.FK_DishStore;
import com.epam.persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class DishService {

    public List<DishModel> findAll() throws SQLException{
        return new DishDAOImpl().findAll();
    }

    public DishModel findById(String s) throws SQLException{
        return new DishDAOImpl().findById(s);
    }

    public int create(DishModel entity) throws SQLException{
        return new DishDAOImpl().create(entity);
    }

    public int update(DishModel entity) throws SQLException{
        return new DishDAOImpl().update(entity);
    }

    public int delete(String s) throws SQLException{
        return new DishDAOImpl().delete(s);
    }


    }

