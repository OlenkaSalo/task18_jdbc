package com.epam.service;

import com.epam.DAO.implementation.CustumerTableDAOImpl;
import com.epam.DAO.implementation.WaiterDAOImpl;
import com.epam.model.CustumerTableModel;
import com.epam.model.WaiterModel;
import com.epam.persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class WaiterService {
    public List<WaiterModel> findAll() throws SQLException {
        return new WaiterDAOImpl().findAll();
    }

    public WaiterModel findById(Integer integer) throws SQLException{
        return new WaiterDAOImpl().findById(integer);
    }

    public int create(WaiterModel entity) throws SQLException{
        return new WaiterDAOImpl().create(entity);
    }

    public int update(WaiterModel entity) throws SQLException{
        return new WaiterDAOImpl().update(entity);
    }

    public int delete(Integer integer) throws SQLException{
        return new WaiterDAOImpl().delete(integer);
    }
    public int deleteWithMoveOfCustumerTable(Integer idDeleted, Integer idMoveTo) throws SQLException {
        int deletedAmount = 0;
        Connection connection = ConnectionManager.getConnection();
        try {
            connection.setAutoCommit(false);
            if (new WaiterDAOImpl().findById(idMoveTo) == null)
                throw new SQLException();

            List<CustumerTableModel> custumerTable = new CustumerTableDAOImpl().findByWaiterNum(idDeleted);
            for (CustumerTableModel entity : custumerTable) {
                entity.setWaiterNum(idMoveTo);
                new CustumerTableDAOImpl().update(entity);
            }
            deletedAmount = new WaiterDAOImpl().delete(idDeleted);
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                System.err.print("Transaction is being rolled back");
                connection.rollback();
            }
        } finally {
            connection.setAutoCommit(true);
        }
        return deletedAmount;
    }
}
