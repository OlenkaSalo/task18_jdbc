package com.epam.service;

import com.epam.DAO.implementation.CustumerTableDAOImpl;
import com.epam.model.CustumerTableModel;


import java.sql.SQLException;
import java.util.List;

public class CustumerTableService {
    public List<CustumerTableModel> findAll() throws SQLException {
        return new CustumerTableDAOImpl().findAll();
    }

    public CustumerTableModel findById(Integer integer) throws SQLException{
        return new CustumerTableDAOImpl().findById(integer);
    }

    public int create(CustumerTableModel entity) throws SQLException{
        return new CustumerTableDAOImpl().create(entity);
    }

    public int update(CustumerTableModel entity) throws SQLException{
        return new CustumerTableDAOImpl().update(entity);
    }

    public int delete(Integer integer) throws SQLException{
        return new CustumerTableDAOImpl().delete(integer);
    }


}
