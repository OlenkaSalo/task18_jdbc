package com.epam.DAO;

import com.epam.model.DishModel;
import com.epam.model.FK_DishStore;

public interface DishStoreDAO extends GeneralDAO<FK_DishStore, FK_DishStore> {
}
