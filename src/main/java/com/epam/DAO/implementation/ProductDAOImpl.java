package com.epam.DAO.implementation;

import com.epam.DAO.ProductDAO;
import com.epam.model.ProductModel;
import com.epam.persistant.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAOImpl implements ProductDAO {

    private static final String FIND_ALL = "SELECT * FROM product ";
    private static final String FIND_BY_ID = "SELECT * FROM product WHERE prod_name=?";
    private static final String DELETE = "DELETE FROM product WHERE prod_name=?";
    private static final String CREATE = "INSERT product(prod_name) VALUES (?)";
    private static final String UPDATE = "UPDATE product WHERE prod_name=?";


    @Override
    public List<ProductModel> findAll() throws SQLException {
        List<ProductModel> products = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()) {
            try (ResultSet rs = statement.executeQuery(FIND_ALL)) {
                while (rs.next()) {
                    products.add((ProductModel) new Transformer<>(ProductModel.class).fromResultSetModel(rs));
                }
            }
        }
        return products;
    }

    @Override
    public ProductModel findById(String s) throws SQLException {
        ProductModel model = null;
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)){
            ps.setString(1,s);
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    model = (ProductModel) new Transformer<>(ProductModel.class).fromResultSetModel(rs);
                }
            }
        }
        return  model;
    }

    @Override
    public int create(ProductModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setString(1,entity.getProductName());
            return ps.executeUpdate();

        }
    }

    @Override
    public int update(ProductModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1,entity.getProductName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String s) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setString(1,s);
            return ps.executeUpdate();

        }
    }
}