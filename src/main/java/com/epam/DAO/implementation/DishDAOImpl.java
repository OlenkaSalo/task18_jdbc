package com.epam.DAO.implementation;

import com.epam.DAO.DishDAO;
import com.epam.model.DishModel;
import com.epam.persistant.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DishDAOImpl implements DishDAO {

    private static final String FIND_ALL = "SELECT * FROM dish";
    private static final String FIND_BY_ID="sELECT * FROM dish WHERE dish_name=?";
    private static final String CREATE = "INSERT dish (dish_name, price, type_in_menu) values(?,?,?)";
    private static final String UPDATE ="UPDATE dish SET price=?, type_in_menu=? WHERE dish_name=?";
    private static final String DELETE ="DELETE FROM dish WHERE dish_name=?";

    @Override
    public List<DishModel> findAll() throws SQLException {
        List<DishModel> dish  =new ArrayList<DishModel>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet resultSet = statement.executeQuery(FIND_ALL)){
                while(resultSet.next()){
                    dish.add((DishModel) new Transformer(DishModel.class).fromResultSetModel(resultSet));
                }
            }

        }
    return dish;
    }

    @Override
    public DishModel findById(String s) throws SQLException {
       DishModel model = null;
       Connection connection = ConnectionManager.getConnection();
       try(PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)){
           ps.setString(1, s);
           try (ResultSet resultSet = ps.executeQuery()){
               while(resultSet.next()){
                   model=(DishModel) new Transformer<>(DishModel.class).fromResultSetModel(resultSet);
                   break;
               }
           }
       }
       return model;
    }

    @Override
    public int create(DishModel entity) throws SQLException {
       Connection connection = ConnectionManager.getConnection();
       try (PreparedStatement ps  = connection.prepareStatement(CREATE)){
           ps.setString(1,entity.getDishName());
           ps.setBigDecimal(2,entity.getPrice());
           ps.setString(3,entity.getTypeInMenu());
           return ps.executeUpdate();
       }
    }

    @Override
    public int update(DishModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps  = connection.prepareStatement(UPDATE)){
            ps.setBigDecimal(1,entity.getPrice());
            ps.setString(2,entity.getTypeInMenu());
            ps.setString(3,entity.getDishName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String s) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setString(1,s);
            return ps.executeUpdate();
        }
    }
}
