package com.epam.DAO.implementation;

import com.epam.DAO.CustumerTableDAO;
import com.epam.DAO.DishAndCustomerDAO;
import com.epam.model.FK_DishAndCustomer;
import com.epam.persistant.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DishAndCustumerDAOImpl implements DishAndCustomerDAO {

    private static final String FIND_ALL = "SELECT * FROM dish_and_custumer";
    private static final String CREATE = "INSERT dish_and_custumer (dish_name, id_table, order_amount) values(?,?,?)";
    private static final String UPDATE ="UPDATE dish_and_custumer SET dish_name=?, price=?, type_in_menu=?";
    private static final String DELETE ="DELETE * FROM dish_and_custumer";

    @Override
    public List<FK_DishAndCustomer> findAll() throws SQLException {
        List<FK_DishAndCustomer> order = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet rs = statement.executeQuery(FIND_ALL)){
                while(rs.next()){
                    order.add((FK_DishAndCustomer) new Transformer<>(FK_DishAndCustomer.class).fromResultSetModel(rs));
                }
            }
        }
        return order;
    }

    @Override
    public FK_DishAndCustomer findById(FK_DishAndCustomer entity) throws SQLException {
        return null;
    }

    @Override
    public int create(FK_DishAndCustomer entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setString(1,entity.getDishName());
            ps.setInt(2,entity.getTableNum());
            ps.setString(3, entity.getOrderAmount());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(FK_DishAndCustomer entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(UPDATE)){
            ps.setString(1,entity.getDishName());
            ps.setInt(2,entity.getTableNum());
            ps.setString(3, entity.getOrderAmount());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(FK_DishAndCustomer entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setString(1,entity.getDishName());
            ps.setInt(2,entity.getTableNum());
            ps.setString(3, entity.getOrderAmount());
            return ps.executeUpdate();
        }
    }
}
