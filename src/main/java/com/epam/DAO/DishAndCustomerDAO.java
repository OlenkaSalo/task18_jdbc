package com.epam.DAO;

import com.epam.model.FK_DishAndCustomer;

public interface DishAndCustomerDAO extends GeneralDAO<FK_DishAndCustomer, FK_DishAndCustomer> {
}
