package com.epam.DAO;

import com.epam.model.WaiterModel;

public interface WaiterDAO extends GeneralDAO<WaiterModel, Integer> {
}
