package com.epam.DAO;

import com.epam.model.CustumerTableModel;

import java.sql.SQLException;
import java.util.List;

public interface CustumerTableDAO extends GeneralDAO<CustumerTableModel, Integer> {

    List<CustumerTableModel> findByWaiterNum(Integer waiterNum) throws SQLException;
}
