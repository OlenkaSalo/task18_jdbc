package com.epam.DAO;

import com.epam.model.DishModel;

public interface DishDAO extends GeneralDAO<DishModel,String> {
}
