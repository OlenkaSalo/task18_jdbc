package com.epam.DAO;

import com.epam.model.ProductModel;

public interface ProductDAO extends GeneralDAO<ProductModel, String> {
}
