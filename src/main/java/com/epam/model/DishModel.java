package com.epam.model;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.PrimaryKey;
import com.epam.model.Annotation.Table;

import java.math.BigDecimal;


@Table(name = "dish")
public class DishModel {
    @PrimaryKey
    @Column(name="dish_name", length = 40)
    private String dishName;
    @Column(name="price", length = 10)
    private BigDecimal price;
    @Column(name="type_in_menu", length = 10)
    private String typeInMenu;

    public DishModel(){}

    public DishModel(String dishName,BigDecimal price, String typeInMenu)
    {
        this.dishName=dishName;
        this.price=price;
        this.typeInMenu=typeInMenu;
    }

    public String getDishName(){return dishName;}

    public void setDishName(String dishName){this.dishName=dishName;}

    public BigDecimal getPrice(){return price;}

    public void setPrice(BigDecimal price){this.price=price;}

    public String getTypeInMenu(){return typeInMenu;}

    public void setTypeInMenu(String typeInMenu){this.typeInMenu=typeInMenu;}

    @Override
    public String toString()
    {
        return String.format("%-5s %4.5f  %s", dishName, price,typeInMenu );
    }



}
