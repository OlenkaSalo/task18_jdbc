package com.epam.model;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.PrimaryKey;
import com.epam.model.Annotation.Table;

@Table(name="product")
public class ProductModel {
    @PrimaryKey
    @Column(name = "prod_name", length = 25)
    private String productName;

    public ProductModel(){}

    public ProductModel(String productName)
    {
        this.productName=productName;
    }
    public String getProductName(){return productName;}

    public void setProductName(String productName){this.productName=productName;}

    @Override
    public String toString(){return String.format("%-5s",productName);}
}
