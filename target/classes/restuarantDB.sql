

create table Dish(
    dish_name      varchar(25) primary key,
    price          decimal(10,2),
    dish_number    int
)engine = InnoDB;

create table Product(
 prod_name          varchar(25) primary key
)engine = InnoDB;


create table Dish_store(
  dish_name    varchar(25) ,
  prod_name    varchar(25)  ,
  number       int

) engine=InnoDB;

create table Custumer_table(
id_table            int primary key,
ordertime           time,
id_waiter           int

)engine = InnoDB;

create table Dish_and_Custumer(
dish_name   varchar(25),
id_table  int,
order_amount  decimal(10,3)
) engine = InnoDB;

create table Waiter(
id_waiter   int primary key,
name        varchar(10),
lastname    varchar(10)
)engine=InnoDB;


alter table Dish_store
add constraint fk_Dish_store_Dish
foreign key (dish_name)
references Dish (dish_name)
on delete cascade on update set null;

alter table Dish_store
add constraint fk_Dish_store_Product
foreign key (prod_name)
references Product (prod_name)
on delete cascade on update set null;


alter table Custumer_table
add constraint fk_table_waiter
foreign key (id_waiter)
references Waiter  (id_waiter)
on delete cascade on update set null;

alter table Dish_and_Custumer
add constraint fk_order_table
foreign key (id_table)
references Custumer_table (id_table)
on delete cascade on update set null;

alter table Dish_and_Custumer
add constraint fk_order_dish
foreign key (dish_name)
references Dish (dish_name)
on delete cascade on update set null;
